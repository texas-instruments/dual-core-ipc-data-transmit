# [Dual Core IPC Data transmit](https://dev.ti.com/tirex/explore/content/c2000_academy_2_02_01_42_all/modules/c2000_academy_lab/advanced_topics/ipc_lab/c2000_lab_ipc.html)

# Objective 
The objective of this lab exercise is to become familiar with the operation of the IPC module. We will be using the basic IPC features to send data in both directions between CPU1 and CPU2. We will also be using an ePWM module to continuously trigger conversions on an ADC module, which triggers an IPC interrupt at the end of the conversion. At the same time, a DAC will feed data into the ADC, with the help of one of the IPC interrupts. In addition to exploring a basic IPC example, this lab should leave readers more knowledgeable about the capabilities of C2000's ePWM, DAC, and ADC modules.

## Note:
Inter Processor Communication (IPC) module is available only on dual-core systems TMS320F2838xD and TMS320F2837xD

# Solution
All solutions are available in the directory: [C2000Ware_Install_Path]/training/device/[device_name]/.

# Introduction
Like some other lab exercises, ePWM2 will be configured to provide a 50 kHz SOC signal to ADCA to trigger a conversion. An End-of-Conversion ISR on CPU1 will write each result into a data register in the C1toC2 IPC registers. An IPC interrupt will then be triggered on CPU2 which fetches this data and stores it in a circular buffer. The same ISR on CPU2 grabs a data point from a sine table and loads it into a data register in the C2toC1 IPC registers for transmission to CPU1. This triggers an interrupt on CPU1, which fetches the sine data and writes it into DACB. The DACB output is connected to ADCA by a jumper wire. If the program runs as expected, the sine table and ADC results buffer on CPU2 should contain very similar data.

## IPC Data Flow
In summary:

1. CPU1 will send ADC samples to CPU2 via the IPC registers at a rate of 50kHz which triggers an IPC0 ISR on CPU2.
1. CPU2 IPC0 ISR will retrieve samples from the IPC registers and store them in circular buffer.
1. CPU2 IPC0 will then send the next sinusoidal lookup table entry back to CPU1 via the IPC registers which triggers an IPC1 ISR on CPU1.
1. CPU1 IPC1 ISR will then retrieve the DAC sample from the IPC register and load it onto the DAC.
1. This process repeats and you should be able to view the sinusoidal waveform via the internal buffer AdcBuf using the plot functionality. Note that AdcBuf has a size of 256 and is of type uint16_t. The sampling rate is 50kHz.


Note: LED1 and LED2 are being toggled by CPU1 and CPU2 in their respective IPC ISRs. They should be toggling at a rate of 1Hz and should have alternating states, i.e., if LED1 is high, then LED2 should be low, etc.

# Lab Setup
## Hardware Setup
You will need the following hardware for this lab:

- A C2000 controlCARD or LaunchPad with the supplied USB cable.
- Jumper cables.
- Oscilloscope (optional).

Use the supplied USB cable to connect your C2000 board's USB port to the standard USB Type-A connector in your computer. You should see some LEDs light up on your board. In addition to powering the board, a JTAG communication link is also established between the device and Code Composer Studio. Refer to Getting Started module for more details if needed.

Connect the jumper cable from the ADCINA0 pin to the DACB pin so the ADC can sample the output of the DAC.

LaunchPad	controlCARD
Device	ADCINA0 Pin	DACB Pin	ADCINA0 Pin	DACB Pin
F2837xd	30	70	9	11
F2838x	n/a	n/a	9	11
## Software Setup
The following software will need to be installed on your computer:
- Code Composer Studio
- C2000Ware
## Import a new driverlib project

Our first task is to import an empty driverlib project in our Code Composer Studio (CCS) workspace. The basic instructions are as follows:

1. Open CCS and go to Project→Import CCS Projects. A new window should appear. Ensure that the Select search-directory option is activated.
1. Click the Browse button and select the C:/ti/c2000/C2000Ware_X_XX_00_00/driverlib/<devicename>/examples/dual/empty_projects/CCS directory. Note that we assume the default Windows C2000Ware install path.
1. Under Discovered Projects, you should now see the empty_driverlib_project_cpu1project. Select this project by clicking the checkbox to the left and ensure that it is the only project selected.
1. Click Finish to import and copy the empty_driverlib_project_cpu1 project into your workspace.
1. You should see two projects empty_driverlib_project_cpu1 and empty_driverlib_project_cpu2
1. Rename empty_driverlib_project_cpu1 project to lab_ex_cpu1 and rename the empty_driverlib_project_cpu1.c file to lab_ex_main_cpu1.c. Repeat it again for CPU2.
1. Copy and paste the ipc.h and ipc.c files into the device folder in your project directory from C:\ti\c2000\C2000Ware_4_01_00_00\training\device\2837xd\module11_inter_processor_communications. The 2838x device folder does not contain these files. 
Note: C2000Ware_4_01_00_00 will change depending on installed version. C:\ti\c2000\C2000Ware_4_02_00_00\training\device\f2837xd\advance_topics\lab_ipc
1. In the lab_ex_cpu2, go to Project→Properties→Build→C2000 Compiler→Predefined Symbols and change the CPU1 symbol to be CPU2.

# Part 1 : CPU1 (C28x)

## Define global macros and variables
First, we will define some necessary macros and global variables. Click on the .c file in your project. In order to use driverlib, make sure that you have included driverlib.h, device.h, and ipc.h. The files driverlib.h and device.h should be included by default. A description of the following macros and global variables will be addressed in the following sections.

## Define main()
Next, we will populate main() as shown below. Details about the configuration of interrupts, PIE, PLL, and watchdog timer are discussed in detail in Boot-mode, Resets, Interrupts and Non Maskable Interrupts (NMI) and CPU Timers, Watchdog timer, GPIO and Crossbars. That being said, we will still need to know the system clock frequency in order to configure our PWM waveform frequency. The system clock frequency value is defined via the macro DEVICE_SYSCLK_FREQ in [projectroot]/device/device.h. Observe that the main function only handles initialization routines. Most of the activity in this lab lies in the peripherals themselves and their interrupt service routines.

## Configure the GPIO
Next, we will configure the necessary GPIO pins as shown below. Two LEDs (LED1 and LED2) are used for visual indication of IPC communication. LED1 is toggled by CPU1 whereas LED2 is toggled by CPU2. The GPIO pins for the two LEDs are configured, and CPU2 is made as GPIO master for LED2 so that it can toggle the pin.

## Configure ePWM2A
In this section, we will first configure ePWM2A to output a 50 kHz SOC signal to ADC-A.

## Configure the ADC
In the previous section, we explained that ePWM2 would be triggering a SOC event on the ADC. In this section, we will provide the code to configure the ADC. More details about the configuration of the ADC can be found in Analog-to-Digital Converter (ADC). However, notice that we have setup a SOC to be triggered by ePWM2 and that we have setup the ADC to interrupt at the end of a conversion. Hence, the interrupt service routine will be triggered at a rate of 50kHz. The ADC is also setup in continuous mode so that the ADC register always contains the most recent sample.

## Define the ADC interrupt service routine
In this section, we will define the ADC interrupt service routine. This interrupt service routine will read the result and write it into the data register of the IPC module. It uses IPC DriverLib API to write the result to the data register. Then an IPC interrupt will be triggered on CPU2.

## Configure DAC
In this section, we will configure DAC-B. We will set the reference voltage and set load mode clock as system clock. The DAC-B output is connected by a jumper wire to the ADCINA0 pin.

## Define the IPC interrupt service routine
The interrupt service routine of the IPC reads the data from the IPC data register using the IPC driverlib API. The read data is then loaded into the shadow register of the DAC-B.

Now save the files and build the project.

# Part 2 : CPU2 (C28x)

## Define global macros and variables
First, we will define some necessary macros and global variables. In order to use driverlib, make sure that you have included driverlib.h, device.h and ipc.h.

There are two main global variables:

- SinTable - The SinTable consists of fix-point 16-bit samples.
- AdcBuf - It is circular buffer which is filled by IPC data received from CPU1. AdcBuf is used to plot the graph.

## Define main()
Next, we will populate main() as shown below. Details about the configuration of interrupts, PIE, PLL, and watchdog timer are discussed in detail in Boot-mode, Resets, Interrupts and Non Maskable Interrupts (NMI) and CPU Timers, Watchdog timer, GPIO and Crossbars.

Besides system initialization, we will register IPC interrupt service routine for IPC flag0.

## Define the IPC interrupt service routine
The interrupt service routine of the IPC reads the data from the IPC data register using the IPC driverlib API. The read data is then stored in the AdcBuf circular buffer . Then it reads the next data from the SinTable and writes it to the IPC data register for CPU1. Again, we will use the driverlib API to write to the IPC register.

Now save the files and build the project.

## Jumper Pin Connection
Connect the jumper cable from the ADCINA0 pin to the DACB pin so the ADC can sample the output of the DAC.

| LaunchPad |             | controlCARD |             |          |
|-----------|-------------|-------------|-------------|----------|
| Device    | ADCINA0 Pin | DACB Pin    | ADCINA0 Pin | DACB Pin |
| F2837xd   | 30          | 70          | 9           | 11       |
| F2838x    | n/a         | n/a         | 9           | 11       |

## Build and Load the Project

- In the Project Explorer window click on the lab_ex_cpu1 project to set it active. Then click the “Build” button and watch the tools run in the “Console” window. Check for any errors in the “Problems” window. Repeat this step for the lab_ex_cpu2 project for CPU2.
- Again, in the Project Explorer window click on the lab_ex_cpu1 project to set it active. Click on the “Debug” button (green bug). A Launching Debug Session window will open. Select only CPU1 to load the program on, and then click OK. The “CCS Debug” perspective view should open, then CPU1 will connect to the target and the program will load automatically.
- Next, we need to connect to and load the program on CPU2. Right-click at the line Texas Instruments XDS100v2 USB Debug Probe_0/C28xx_CPU2 and select Connect Target.
- With the line Texas Instruments XDS100v2 USB Debug Probe_0/C28xx_CPU2 still highlighted, load the program: 'Run > Load > Load Program…' Browse to the file: [project]/CPU2_RAM/lab_ex_cpu2.out and select OK to load the program.
- Again, with the line Texas Instruments XDS100v2 USB Debug Probe_0/C28xx_CPU2 still highlighted, set the bootloader mode using the menu bar by clicking: 'Scripts > EMU Boot Mode Select > EMU_BOOT_SARAM'. CPU1 bootloader mode was already set in the previous lab exercise. If the device has been power cycled since the last lab exercise, be sure to configure the boot mode to EMU_BOOT_SARAM using the Scripts menu for both CPU1 and CPU2.

## Run the Code
- In the Debug window, click on the line Texas Instruments XDS100v2 USB Debug Probe_0/C28xx_CPU1. Run the code on CPU1 by clicking the green “Resume” button. At this point CPU1 is waiting for CPU2 to be ready. In the Debug window, click on the line Texas Instruments XDS100v2 USB Debug Probe_0/C28xx_CPU2. As before, run the code on CPU2 by clicking the “Resume” button. Using the IPC, CPU2 communicates to CPU1 that it is now ready. LED D10 connected to CPU1 on the LaunchPad should be blinking at a period of approximately 1 second. Note that LED D9 connected to CPU2 will not be used in this lab exercise.
- In the Debug window select CPU1. Halt the CPU1 code after a few seconds by clicking on the “Suspend” button.
- Then in the Debug window select CPU2. Halt the CPU2 code by using the same procedure.

## View the Circular Buffer (AdcBuf)
- Open and set up a graph to plot a 256-point window of the ADC results buffer. Click: Tools→Graph→Single Time and set the following values:
- Select OK to save the graph options.
- If the IPC communications are working, the ADC buffer on CPU2 should contain the sine data transmitted from the look-up table. The graph view should look like:
- We will now run the code in real-time emulation mode. On the graph window toolbar, left-click on Enable Continuous Refresh (the yellow icon with the arrows rotating in a circle over a pause sign). This will allow the graph to continuously refresh in real-time while the program is running.

# IPC communication between C28x and CM Core 
Note:
TMS320F2838xD also has Connectivity Manager (CM) subsystem. The CM subsystem is based on the industry-standard 32-bit Arm® Cortex®-M4 CPU and features a wide variety of communication peripherals, including EtherCAT, Ethernet, USB, MCAN (CAN-FD), DCAN, UART, SSI, I2C, and so on. The IPC communication between C28x and CM core functions in a similar way as IPC communication between CPU1 and CPU2 as illustrated above. All IPC features are independent of each other, and have dedicated IPC MSGRAM and IPC flags. Please refer to the C2000Ware DriverLib example if you need working example for IPC communication between C28x and CM core. The example can be found at the following location: C:/ti/c2000/C2000Ware_X_XX_XX_XX/driverlib/f2838x/examples/c28x_cm/ipc

# Full Solution
The full solution to this lab exercise is included as part of the C2000Ware SDK. Import the project from [C2000Ware_Install_Path]/training/device/[device_name]/advance_topics/lab_ipc.

